var angular = require('angular');

module.exports =
    angular.module('dd-http-wrapper', [])
        .provider('ddHttpService', require('./services/ddHttpServiceProvider'))
        .name;