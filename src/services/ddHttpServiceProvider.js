module.exports = function ddHttpServiceProvider () {
    var baseUrl = null;
    var header = null;

    this.setBaseUrl = function(val) {
        baseUrl = val;
    };
    this.setHeader = function(val) {
        header = val;
    };

    this.$get = ['$log', '$q', '$http', function ddHttpServiceProviderFactory($log, $q, $http) {
        /**
         * Resolving the url to call based on configuration.
         * @param relativeUrl
         * @returns string
         */
        resolvePath = function(relativeUrl) {
            return (baseUrl) ? baseUrl + relativeUrl : relativeUrl;
        };

        return {
            /**
             * Sends a GET request to the provided relative uri.
             * @param relativeUrl
             * @param params
             * @param cache
             * @returns {Promise}
             */
            get: function (relativeUrl, params, cache) {
                var target = resolvePath(relativeUrl);
                var useCache = (cache) ? cache : true;

                $log.debug('Sending GET request to: ' + target);

                var deferred = $q.defer();
                $http.get(target, {
                    params: params,
                    cache: useCache,
                    headers: header
                }).then(function success(response) {
                    $log.debug('GET request to ' + target + ' executed successfully with response', response);
                    deferred.resolve(response);

                }, function error(response) {
                    $log.debug('GET request to ' + target + ' failed execution with response', response);
                    deferred.reject(response);
                });

                return deferred.promise;
            },

            /**
             * Sends a POST request to the provided relative URI
             * @param relativeUrl
             * @param data
             * @param cache
             * @returns {Promise}
             */
            post: function(relativeUrl, data, cache) {
                var target = resolvePath(relativeUrl);
                var useCache = (cache) ? cache : true;

                $log.debug('Sending POST request to: ' + target);

                var deferred = $q.defer();
                $http.post(target, data, {
                    cache: useCache,
                    headers: header
                }).then(function success(response) {
                    $log.debug('POST request to ' + target + ' executed successfully with response', response);
                    deferred.resolve(response);

                }, function error(response) {
                    $log.debug('POST request to ' + target + ' failed execution with response', response);
                    deferred.reject(response);
                });

                return deferred.promise;
            }
        };
    }]
};