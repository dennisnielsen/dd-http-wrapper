const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: './src/dd-http-wrapper.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'dd-http-wrapper.min.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /(node_modules)/,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false,
                sequences: true,
                conditionals: true,
                booleans: true,
                if_return: true,
                join_vars: true,
                drop_console: true
            },
            output: {
                comments: false
            },
            minimize: true
        })
    ]
};